package com.sgi.synchrestapi.controller;

import com.sgi.synchrestapi.InvoiceService;
import com.sgi.synchrestapi.common.Invoice;
import com.sgi.synchrestapi.messaging.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class InvoiceController {
    private final Logger logger = LoggerFactory.getLogger(InvoiceService.class);

    @PostMapping(path = "/api/invoice")
    public String processInvoice(@RequestBody Invoice newInvoice) {
        logger.info("process new invoice");
        logger.info(newInvoice);
        // Produce event
        Producer invoiceProducer = new Producer();
        invoiceProducer.sendInvoice(newInvoice);

        // Wait result from consume
        // How??

        return "Invoice proceed";
    }
}
